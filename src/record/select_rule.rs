/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

// FIXME: Experimental breakup of select rule information

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum AiSetting {
    GreetingDistance,
    FightChance,
    FleeChance,
    AlarmChance,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum SelectFunction {
   RankLow, // 0
   RankHigh, // 1
   RankRequirement, // 2
   Reputation, // 3
   HealthPercent, // 4
   PcReputation, // 5
   PcLevel, // 6
   PcHealthPercent, // 7
   PcDynamicStat(StatType), // 64: Health, 8: Magicka, 9: Fatigue
   PcAttribute(AttributeType), // 10: Strength, 51-57: Int-Luck
   PcSkill(SkillType), // SkillType - 11
   PcGender, // 38
   PcExpelled, // 39
   PcCommonDisease, // 40
   PcBlightDisease, // 41
   PcClothingModifier, // 42
   PcCrimeLevel, // 43
   SameGender, // 44
   SameRace, // 45
   SameFaction, //46
   FactionRankDiff, // 47;
   Detected, // 48
   Alarmed, // 49
   Choice, // 50
   PcCorprus, // 58
   Weather, // 59
   PcVampire, // 60
   Level, // 61
   Attacked, // 62
   TalkedToPc, // 63
   CreatureTargetted, // 65
   FriendlyHit, // 66
   AI(AiSetting), // 68: Hello, 67: Fight, 70: Flee, 69: Alarm
   ShouldAttack, // 71
   Werewolf, // 72
   WerewolfKills, // 73
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum SelectFunctionType {
    None,                     // '0'
    Function(SelectFunction), // '1'
    /// Tests against the value of a Global record
    Global,                   // '2?X', where ? can be f, s, or l for short, float or long variable
    /// FIXME: Local variables related to an object?
    Local,                    // '3?X'
    Journal,                  // '4JX'
    Item,                     // '5IX'
    Dead,                     // '6DX'
    NotId,                    // '7XX'
    NotFaction,               // '8FX'
    NotClass,                 // '9CX'
    NotRace,                  // 'ARX'
    NotCell,                  // 'BLX'
    NotLocal,                 // 'CsX'
}

fn parse_function(input: &[u8; 3]) -> Result<SelectFunctionType, RecordError> {
    use SelectFunctionType::*;
    use SelectFunction::*;
    Ok(match input {
        b"000" => SelectFunctionType::None,
        b"2sX" => Global,
        b"2fX" => Global,
        b"3sX" => Local,
        b"3fX" => Local,
        b"4JX" => Journal,
        b"5IX" => Item,
        b"6DX" => Dead,
        b"7XX" => NotId,
        b"8FX" => NotFaction,
        b"9CX" => NotClass,
        b"ARX" => NotRace,
        b"BLX" => NotCell,
        b"CsX" => NotLocal,
        x if std::str::from_utf8(x).is_ok() => {
            let num: u8 = std::str::from_utf8(x).unwrap().parse().map_err(|_| RecordError::InvalidSelectRuleType(*x))?;
            Function(match num {
                100 => RankLow,
                101 => RankHigh,
                102 => RankRequirement,
                103 => Reputation,
                104 => HealthPercent,
                105 => PcReputation,
                106 => PcLevel,
                107 => PcHealthPercent,
                164 => PcDynamicStat(StatType::Health),
                108 => PcDynamicStat(StatType::Mana),
                109 => PcDynamicStat(StatType::Fatigue),
                110 => PcAttribute(AttributeType::Strength),
                151..=157 => PcAttribute(FromPrimitive::from_u8(num - 150).unwrap()),
                111..=137 => PcSkill(FromPrimitive::from_u8(num - 111).unwrap()),
                138 => PcGender,
                139 => PcExpelled,
                140 => PcCommonDisease,
                141 => PcBlightDisease,
                142 => PcClothingModifier,
                143 => PcCrimeLevel,
                144 => SameGender,
                145 => SameRace,
                146 => SameFaction,
                147 => FactionRankDiff,
                148 => Detected,
                149 => Alarmed,
                150 => Choice,
                158 => PcCorprus,
                159 => Weather,
                160 => PcVampire,
                161 => Level,
                162 => Attacked,
                163 => TalkedToPc,
                165 => CreatureTargetted,
                166 => FriendlyHit,
                167 => AI(AiSetting::FightChance),
                168 => AI(AiSetting::GreetingDistance),
                169 => AI(AiSetting::AlarmChance),
                170 => AI(AiSetting::FleeChance),
                171 => ShouldAttack,
                172 => Werewolf,
                173 => WerewolfKills,
                _ => return Err(RecordError::InvalidSelectRuleType(*x)),
            })
        }
        x => return Err(RecordError::InvalidSelectRuleType(*x)),
    })
}

