/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, push_opt_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::{Constructor, Display};
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_u8;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(
    Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize, FromPrimitive,
)]
pub enum BodyPartType {
    Head = 0,
    Hair = 1,
    Neck = 2,
    Chest = 3,
    Groin = 4,
    Hand = 5,
    Wrist = 6,
    Forearm = 7,
    Upperarm = 8,
    Foot = 9,
    Ankle = 10,
    Knee = 11,
    Upperleg = 12,
    Clavicle = 13,
    Tail = 14,
}

#[derive(
    Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize, FromPrimitive,
)]
pub enum MeshType {
    Skin = 0,
    Clothing = 1,
    Armor = 2,
}

#[derive(
    Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize, FromPrimitive,
)]
pub enum Gender {
    Male,
    Female,
}

/// Sections of a character's body
#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct BodyPart {
    /// Model identifier
    model: Option<String>,
    /// Race the body part belongs to
    race: Option<String>,
    /// Type of body part
    part: BodyPartType,
    /// Gender of the BodyPart
    gender: Gender,
    /// Indicates body part is playable
    /// (FIXME: Does this mean it shows up in the character generator?)
    playable: Option<bool>,
    /// Indicates body part is vampires
    vampire: Option<bool>,
    /// Type of mesh
    mesh_type: MeshType,
}

impl TryFrom<esplugin::Record> for RecordPair<BodyPart> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<BodyPart>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut model = None;
        let mut race = None;
        let mut part = None;
        let mut mesh_type = None;
        let mut vampire = None;
        let mut playable = None;
        let mut gender = Gender::Male;
        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => race = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"BYDT" => {
                    let (data, part_num) = le_u8(data)?;
                    let (data, vampire_num) = le_u8(data)?;
                    let (data, flags) = le_u8(data)?;
                    let (_, mesh_type_num) = le_u8(data)?;
                    part = Some(
                        FromPrimitive::from_u8(part_num)
                            .ok_or(RecordError::InvalidBodyPart(part_num))?,
                    );
                    if vampire_num != 0 {
                        vampire = Some(true);
                    }
                    if flags & 1 != 0 {
                        gender = Gender::Female;
                    }
                    if flags & 2 != 0 {
                        playable = Some(true);
                    }
                    mesh_type = Some(
                        FromPrimitive::from_u8(mesh_type_num)
                            .ok_or(RecordError::InvalidMeshType(mesh_type_num))?,
                    );
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let part = part.ok_or(RecordError::MissingSubrecord("BYDT"))?;
        let mesh_type = mesh_type.ok_or(RecordError::MissingSubrecord("BYDT"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::BodyPart, id),
            BodyPart::new(model, race, part, gender, playable, vampire, mesh_type),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<BodyPart> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));
        push_opt_str_subrecord(&mut subrecords, &self.record.race, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;

        let mut data = vec![];
        data.extend(&(self.record.part as u8).to_le_bytes());
        if let Some(true) = self.record.vampire {
            data.extend(&(1u8).to_le_bytes());
        } else {
            data.extend(&(0u8).to_le_bytes());
        }
        let mut flags: u8 = 0;
        if self.record.gender == Gender::Female {
            flags |= 1;
        }
        if let Some(true) = self.record.playable {
            flags |= 2;
        }
        data.extend(&flags.to_le_bytes());
        data.extend(&(self.record.mesh_type as u8).to_le_bytes());
        subrecords.push(Subrecord::new(*b"BYDT", data, false));

        let header = RecordHeader::new(*b"BODY", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
