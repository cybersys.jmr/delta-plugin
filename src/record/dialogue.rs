/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::delta::{DeltaOf, DeltaUnionNoDelete};
use crate::float::Float;
use crate::record::{Pair, RecordId, RecordPair, RecordTypeName};
use crate::types::{AttributeType, Gender, SkillType, StatType};
use crate::util::{
    is_default, is_empty, nt_str, push_opt_str_subrecord, record_type, subrecords_len, take_nt_str,
    RecordError,
};
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom::number::complete::{le_f32, le_i32, le_i8};
use nom_derive::Parse;
use num_traits::{FromPrimitive, ToPrimitive};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, FromPrimitive, Hash, ToPrimitive, Serialize, Deserialize,
)]
pub enum DialogueType {
    Topic = 0,
    Voice = 1,
    Greeting = 2,
    Persuasion = 3,
    Journal = 4,
    Unknown = -1,
}

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, FromPrimitive, Hash, ToPrimitive, Serialize, Deserialize,
)]
pub enum QuestStatus {
    Name,
    Finished,
    Restart,
}

#[skip_serializing_none]
#[derive(FullRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Dialogue {
    pub dialogue_type: DialogueType,
    pub info: LinkedHashMap<String, DialogueInfo>,
}

#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DeltaDialogue {
    pub dialogue_type: Option<DialogueType>,
    #[serde(default, skip_serializing_if = "is_empty")]
    pub info: LinkedHashMap<String, DeltaUnionNoDelete<DialogueInfo, DeltaDialogueInfo>>,
}

impl DeltaDialogue {
    pub fn is_empty(&self) -> bool {
        self.dialogue_type.is_none() && self.info.is_empty()
    }
}

impl Dialogue {
    /// Applies another Dialogue to this Dialogue
    ///
    /// This replaces the override behaviour to match that in Morrowind, where
    /// Dialogue records are only accompanied by new info records.
    pub fn apply(&mut self, other: &Self) {
        self.dialogue_type = other.dialogue_type;

        for (id, value) in other.info.iter() {
            self.info.insert(id.clone(), value.clone());
        }
    }

    pub fn vanilla_diff(&self, other: &Self) -> Option<Self> {
        let mut result = LinkedHashMap::new();
        let origmap = &self.info;
        let newmap = &other.info;

        for (key, value) in newmap.iter() {
            if let Some(orig_value) = origmap.get(key) {
                if orig_value != value {
                    result.insert(key.clone(), value.clone());
                }
            } else {
                result.insert(key.clone(), value.clone());
            }
        }
        if !result.is_empty() {
            Some(Self {
                dialogue_type: self.dialogue_type,
                info: result,
            })
        } else {
            None
        }
    }

    pub fn apply_delta(&mut self, other: DeltaDialogue) {
        dassign!(self.dialogue_type, other.dialogue_type);
        for (key, info) in other.info {
            match info {
                DeltaUnionNoDelete::Modify(info) => {
                    if self.info.contains_key(&key) {
                        if info.deleted.is_some() && info.deleted.unwrap() {
                            self.info.remove(&key);
                        } else {
                            self.info.get_mut(&key).unwrap().apply_delta(info);
                        }
                    }
                }
                DeltaUnionNoDelete::Add(info) => {
                    if self.info.contains_key(&key) {
                        if info.deleted {
                            self.info.remove(&key);
                        } else {
                            self.info.insert(key, info);
                        }
                    }
                }
            }
        }
    }

    pub fn create_delta(&self, other: &Dialogue, _ignore_missing: bool) -> DeltaDialogue {
        let dialogue_type = ddiff!(self.dialogue_type, other.dialogue_type);
        let mut info = LinkedHashMap::new();

        if self.info != other.info {
            for (key, value) in other.info.iter() {
                if let Some(orig_value) = self.info.get(key) {
                    if orig_value != value {
                        let delta = orig_value.create_delta(value, false);
                        info.insert(key.clone(), DeltaUnionNoDelete::Modify(delta));
                    }
                } else {
                    info.insert(key.clone(), DeltaUnionNoDelete::Add(value.clone()));
                }
            }
        }

        DeltaDialogue {
            dialogue_type,
            info,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum AiSetting {
    GreetingDistance,
    FightChance,
    FleeChance,
    AlarmChance,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum SelectFunction {
    RankLow,                    // 0
    RankHigh,                   // 1
    RankRequirement,            // 2
    Reputation,                 // 3
    HealthPercent,              // 4
    PcReputation,               // 5
    PcLevel,                    // 6
    PcHealthPercent,            // 7
    PcDynamicStat(StatType),    // 64: Health, 8: Magicka, 9: Fatigue
    PcAttribute(AttributeType), // 10: Strength, 51-57: Int-Luck
    PcSkill(SkillType),         // SkillType - 11
    PcGender,                   // 38
    PcExpelled,                 // 39
    PcCommonDisease,            // 40
    PcBlightDisease,            // 41
    PcClothingModifier,         // 42
    PcCrimeLevel,               // 43
    SameGender,                 // 44
    SameRace,                   // 45
    SameFaction,                //46
    FactionRankDiff,            // 47;
    Detected,                   // 48
    Alarmed,                    // 49
    Choice,                     // 50
    PcCorprus,                  // 58
    Weather,                    // 59
    PcVampire,                  // 60
    Level,                      // 61
    Attacked,                   // 62
    TalkedToPc,                 // 63
    CreatureTargetted,          // 65
    FriendlyHit,                // 66
    AI(AiSetting),              // 68: Hello, 67: Fight, 70: Flee, 69: Alarm
    ShouldAttack,               // 71
    Werewolf,                   // 72
    WerewolfKills,              // 73
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum SelectFunctionType {
    None,                     // '0'
    Function(SelectFunction), // '1'
    /// Tests against the value of a Global record
    Global, // '2?X', where ? can be f, s, or l for short, float or long variable
    /// FIXME: Local variables related to an object?
    Local, // '3?X'
    Journal,                  // '4JX'
    Item,                     // '5IX'
    Dead,                     // '6DX'
    NotId,                    // '7XX'
    NotFaction,               // '8FX'
    NotClass,                 // '9CX'
    NotRace,                  // 'ARX'
    NotCell,                  // 'BLX'
    NotLocal,                 // 'CsX'
}

/*
fn parse_function(input: &[u8; 3]) -> Result<SelectFunctionType, RecordError> {
    use SelectFunction::*;
    use SelectFunctionType::*;
    Ok(match input {
        b"000" => SelectFunctionType::None,
        b"2sX" => Global,
        b"2fX" => Global,
        b"3sX" => Local,
        b"3fX" => Local,
        b"4JX" => Journal,
        b"5IX" => Item,
        b"6DX" => Dead,
        b"7XX" => NotId,
        b"8FX" => NotFaction,
        b"9CX" => NotClass,
        b"ARX" => NotRace,
        b"BLX" => NotCell,
        b"CsX" => NotLocal,
        x if std::str::from_utf8(x).is_ok() => {
            let num: u8 = std::str::from_utf8(x)
                .unwrap()
                .parse()
                .map_err(|_| RecordError::InvalidSelectRuleType(*x))?;
            Function(match num {
                100 => RankLow,
                101 => RankHigh,
                102 => RankRequirement,
                103 => Reputation,
                104 => HealthPercent,
                105 => PcReputation,
                106 => PcLevel,
                107 => PcHealthPercent,
                164 => PcDynamicStat(StatType::Health),
                108 => PcDynamicStat(StatType::Mana),
                109 => PcDynamicStat(StatType::Fatigue),
                110 => PcAttribute(AttributeType::Strength),
                151..=157 => PcAttribute(FromPrimitive::from_u8(num - 150).unwrap()),
                111..=137 => PcSkill(FromPrimitive::from_u8(num - 111).unwrap()),
                138 => PcGender,
                139 => PcExpelled,
                140 => PcCommonDisease,
                141 => PcBlightDisease,
                142 => PcClothingModifier,
                143 => PcCrimeLevel,
                144 => SameGender,
                145 => SameRace,
                146 => SameFaction,
                147 => FactionRankDiff,
                148 => Detected,
                149 => Alarmed,
                150 => Choice,
                158 => PcCorprus,
                159 => Weather,
                160 => PcVampire,
                161 => Level,
                162 => Attacked,
                163 => TalkedToPc,
                165 => CreatureTargetted,
                166 => FriendlyHit,
                167 => AI(AiSetting::FightChance),
                168 => AI(AiSetting::GreetingDistance),
                169 => AI(AiSetting::AlarmChance),
                170 => AI(AiSetting::FleeChance),
                171 => ShouldAttack,
                172 => Werewolf,
                173 => WerewolfKills,
                _ => return Err(RecordError::InvalidSelectRuleType(*x)),
            })
        }
        x => return Err(RecordError::InvalidSelectRuleType(*x)),
    })
}
*/

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, FromPrimitive, Hash, ToPrimitive, Serialize, Deserialize,
)]
pub enum SelectOp {
    Eq = '0' as isize,
    Ne = '1' as isize,
    Gt = '2' as isize,
    Ge = '3' as isize,
    Lt = '4' as isize,
    Le = '5' as isize,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum SelectResult {
    Int(i64),
    Float(Float), // hashing?
}

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, Hash, FromPrimitive, ToPrimitive, Serialize, Deserialize,
)]
pub enum RuleType {
    None = '0' as isize,
    Function = '1' as isize,
    Global = '2' as isize,
    Local = '3' as isize,
    Journal = '4' as isize,
    Item = '5' as isize,
    Dead = '6' as isize,
    NotId = '7' as isize,
    NotFaction = '8' as isize,
    NotClass = '9' as isize,
    NotRace = 'A' as isize,
    NotCell = 'B' as isize,
    NotLocal = 'C' as isize,
}

/// In game, this is stored as a string
#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct SelectRule {
    /// Not clear if this is used
    index: u8,
    /// Source value used to determine if the dialogue info will be displayed
    /// FIXME: rule_type and function should be combined
    pub rule_type: RuleType,
    pub function: (char, char),
    /// Binary operator applied to the source value
    pub op: SelectOp,
    /// Variable in DialogueInfo::result_script that stores the result of this select rule
    pub var_name: Option<String>,
    /// Result value placed into the variable
    pub result: SelectResult,
}

#[skip_serializing_none]
#[derive(DeltaRecord, Clone, Debug, PartialEq, Serialize, Deserialize, Default)]
pub struct DialogueInfo {
    next: Option<String>,
    prev: Option<String>,
    unknown1: Option<i32>,
    disposition: Option<i32>,
    rank: Option<u8>,
    gender: Option<Gender>,
    pc_rank: Option<u8>,
    unknown2: Option<u8>,
    actor: Option<String>,
    race: Option<String>,
    class: Option<String>,
    faction: Option<String>,
    cell: Option<String>,
    pc_faction: Option<String>,
    response: Option<String>,
    sound: Option<String>,
    result_script: Option<String>,
    quest_status: Option<QuestStatus>,
    select_rules: LinkedHashSet<SelectRule>,
    #[serde(default, skip_serializing_if = "is_default")]
    deleted: bool,
}

impl TryFrom<esplugin::Record> for RecordPair<Dialogue> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<Dialogue>, RecordError> {
        let mut id = None;
        let mut dialogue_type = None;
        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"DATA" => {
                    dialogue_type = if data.len() == 1 {
                        Some(
                            FromPrimitive::from_i8(le_i8(data)?.1)
                                .expect("Cannot deserialize dialogue type"),
                        )
                    } else {
                        Some(DialogueType::Unknown)
                    };
                }
                b"DELE" => (),
                x => return Err(RecordError::UnexpectedSubrecord(*x)),
            }
        }

        let dialogue_type = dialogue_type.ok_or(RecordError::MissingSubrecord("DATA"))?;
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;

        let info = LinkedHashMap::new();
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Dialogue, id),
            Dialogue {
                dialogue_type,
                info,
            },
        ))
    }
}

impl TryFrom<Vec<esplugin::Record>> for RecordPair<Dialogue> {
    type Error = RecordError;
    fn try_from(records: Vec<esplugin::Record>) -> Result<Self, RecordError> {
        let mut iter = records.into_iter();
        let mut dialogue: RecordPair<Dialogue> = iter.next().unwrap().try_into()?;

        for record in iter {
            let dialogue_info: Pair<String, DialogueInfo> = record.try_into()?;
            dialogue
                .record
                .info
                .insert(dialogue_info.id, dialogue_info.value);
        }

        Ok(dialogue)
    }
}

impl TryInto<esplugin::Record> for RecordPair<Dialogue> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;
        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));
        subrecords.push(Subrecord::new(
            *b"DATA",
            (self.record.dialogue_type as i8).to_le_bytes().to_vec(),
            false,
        ));

        let header = RecordHeader::new(*b"DIAL", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}

impl TryInto<Vec<esplugin::Record>> for RecordPair<Dialogue> {
    type Error = RecordError;
    fn try_into(mut self) -> Result<Vec<esplugin::Record>, RecordError> {
        let mut records = vec![];

        // Move info records out of self so that they can be serialized after
        let info_records = self.record.info;
        self.record.info = LinkedHashMap::new();
        records.push(self.try_into()?);
        for (key, info) in info_records {
            records.push(Pair::new(key, info).try_into()?);
        }
        Ok(records)
    }
}

impl TryFrom<esplugin::Record> for Pair<String, DialogueInfo> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let mut id = None;
        let mut prev = None;
        let mut next = None;
        let mut info_data = None;
        let mut actor = None;
        let mut race = None;
        let mut class = None;
        let mut faction = None;
        let mut cell = None;
        let mut pc_faction = None;
        let mut response = None;
        let mut sound = None;
        let mut quest_status = None;
        let mut result_script = None;
        let mut select_rule = None;
        let mut select_rules = LinkedHashSet::new();
        let mut deleted = false;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            unknown1: i32,
            disposition: i32,
            rank: u8,
            gender: i8,
            pc_rank: u8,
            unknown2: u8,
        }

        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"INAM" => id = Some(take_nt_str(data)?.1),
                b"PNAM" => prev = Some(take_nt_str(data)?.1),
                b"NNAM" => next = Some(take_nt_str(data)?.1),
                b"DATA" => {
                    info_data = Some(Data::parse(data)?.1);
                }
                b"ONAM" => actor = Some(take_nt_str(data)?.1),
                b"RNAM" => race = Some(take_nt_str(data)?.1),
                b"CNAM" => class = Some(take_nt_str(data)?.1),
                b"FNAM" => faction = Some(take_nt_str(data)?.1),
                b"ANAM" => cell = Some(take_nt_str(data)?.1),
                b"DNAM" => pc_faction = Some(take_nt_str(data)?.1),
                b"NAME" => response = Some(take_nt_str(data)?.1),
                b"SNAM" => sound = Some(take_nt_str(data)?.1),
                b"QSTN" => quest_status = Some(QuestStatus::Name),
                b"QSTF" => quest_status = Some(QuestStatus::Finished),
                b"QSTR" => quest_status = Some(QuestStatus::Restart),
                b"BNAM" => result_script = Some(take_nt_str(data)?.1),
                b"SCVR" => {
                    // Parse into components
                    select_rule = Some(SelectRule {
                        index: (data[0] as char)
                            .to_digit(10)
                            .expect("Expected a numeric character")
                            as u8,
                        rule_type: FromPrimitive::from_u8(data[1])
                            .expect("could not parse rule type"),
                        function: (data[2] as char, data[3] as char),
                        op: FromPrimitive::from_u8(data[4]).expect("Could not parse op"),
                        var_name: take_nt_str(&data[5..]).ok().map(|x| x.1),
                        result: SelectResult::Int(-1),
                    });
                }
                // Result of previous select rule.
                b"INTV" => {
                    if let Some(mut rule) = select_rule {
                        let (_, result) = le_i32(data)?;
                        rule.result = SelectResult::Int(result as i64);
                        select_rules.insert(rule);
                    } else {
                        return Err(RecordError::MissingSubrecord("SCVR"));
                    }
                    select_rule = None;
                }
                b"FLTV" => {
                    if let Some(mut rule) = select_rule {
                        let (_, result) = le_f32(data)?;
                        rule.result = SelectResult::Float(result.to_string().parse().unwrap());
                        select_rules.insert(rule);
                    } else {
                        return Err(RecordError::MissingSubrecord("SCVR"));
                    }
                    select_rule = None;
                }
                b"DELE" => deleted = true,
                typ => Err(RecordError::UnexpectedSubrecord(*typ))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("INAM"))?;
        let (unknown1, disposition, rank, gender, pc_rank, unknown2) = if let Some(data) = info_data
        {
            (
                Some(data.unknown1),
                Some(data.disposition),
                Some(data.rank),
                Some(
                    FromPrimitive::from_i8(data.gender)
                        .ok_or(RecordError::GenderFromBitsError(data.gender))?,
                ),
                Some(data.pc_rank),
                Some(data.unknown2),
            )
        } else {
            (None, None, None, None, None, None)
        };

        Ok(Pair::new(
            id,
            DialogueInfo {
                next,
                prev,
                unknown1,
                disposition,
                rank,
                gender,
                pc_rank,
                unknown2,
                actor,
                race,
                class,
                faction,
                cell,
                pc_faction,
                response,
                sound,
                result_script,
                quest_status,
                select_rules,
                deleted,
            },
        ))
    }
}

impl TryInto<esplugin::Record> for Pair<String, DialogueInfo> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];

        subrecords.push(Subrecord::new(*b"INAM", nt_str(&self.id)?, false));
        push_opt_str_subrecord(&mut subrecords, &self.value.prev, "PNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.next, "NNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.actor, "ONAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.race, "RNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.class, "CNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.faction, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.cell, "ANAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.pc_faction, "DNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.response, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.sound, "SNAM")?;
        match self.value.quest_status {
            Some(QuestStatus::Name) => subrecords.push(Subrecord::new(*b"QSTN", vec![1], false)),
            Some(QuestStatus::Finished) => {
                subrecords.push(Subrecord::new(*b"QSTF", vec![1], false))
            }
            Some(QuestStatus::Restart) => subrecords.push(Subrecord::new(*b"QSTR", vec![1], false)),
            None => (),
        }
        push_opt_str_subrecord(&mut subrecords, &self.value.result_script, "BNAM")?;
        for select_rule in self.value.select_rules.iter() {
            let mut data = vec![];
            let index_char: char = select_rule.index.to_string().parse()?;
            data.push(index_char as u8);
            data.push(ToPrimitive::to_u8(&select_rule.rule_type).unwrap());
            data.extend(&[select_rule.function.0 as u8, select_rule.function.1 as u8]);
            data.push(ToPrimitive::to_u8(&select_rule.op).unwrap());
            if let Some(var_name) = &select_rule.var_name {
                data.extend(nt_str(&var_name)?);
            }
            subrecords.push(Subrecord::new(*b"SCVR", data, false));
            let (typ, data) = match select_rule.result {
                SelectResult::Int(int) => ("INTV", (int as i32).to_le_bytes()),
                SelectResult::Float(float) => ("FLTV", (float.f32()).to_le_bytes()),
            };
            subrecords.push(Subrecord::new(record_type(typ), data.to_vec(), false));
        }
        if self.value.gender.is_some() {
            let mut data = vec![];
            data.extend(&(self.value.unknown1.unwrap_or(0i32) as i32).to_le_bytes());
            data.extend(&(self.value.disposition.unwrap() as i32).to_le_bytes());
            data.extend(&(self.value.rank.unwrap() as u8).to_le_bytes());
            data.extend(&(self.value.gender.unwrap() as i8).to_le_bytes());
            data.extend(&(self.value.pc_rank.unwrap() as u8).to_le_bytes());
            data.extend(&(self.value.unknown2.unwrap_or(0u8) as u8).to_le_bytes());
            subrecords.push(Subrecord::new(*b"DATA", data, false));
        }

        let header = RecordHeader::new(*b"INFO", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
