/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str, RecordError,
};
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_i32;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(OverrideRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct LandTexture {
    texture: Option<String>,
    index: i32,
}

impl TryFrom<esplugin::Record> for RecordPair<LandTexture> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut index = None;
        let mut texture = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"DATA" => texture = Some(take_nt_str(data)?.1),
                b"INTV" => index = Some(le_i32(data)?.1),
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let index = index.ok_or(RecordError::MissingSubrecord("INTV"))?;
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::LandTexture, id),
            LandTexture { texture, index },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<LandTexture> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.texture, "DATA")?;
        subrecords.push(Subrecord::new(
            *b"INTV",
            self.record.index.to_le_bytes().to_vec(),
            false,
        ));
        let header = RecordHeader::new(*b"LTEX", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
