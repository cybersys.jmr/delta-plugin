/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    is_default, parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len,
    take_nt_str, RecordError,
};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashSet;
use nom::number::complete::{le_f32, le_i16, le_i32, le_u8};
use nom::sequence::tuple;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, FromPrimitive, Hash, ToPrimitive, Serialize, Deserialize,
)]
pub enum WeaponType {
    PickProbe = -4,
    HandToHand = -3,
    Spell = -2,
    None = -1,
    ShortBladeOneHand = 0,
    LongBladeOneHand = 1,
    LongBladeTwoHand = 2,
    BluntOneHand = 3,
    BluntTwoClose = 4,
    BluntTwoWide = 5,
    SpearTwoWide = 6,
    AxeOneHand = 7,
    AxeTwoHand = 8,
    MarksmanBow = 9,
    MarksmanCrossbow = 10,
    MarksmanThrown = 11,
    Arrow = 12,
    Bolt = 13,
}

#[bitflags]
#[repr(u32)]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum WeaponTraits {
    Magical = 0x01,
    Silver = 0x02,
}

impl Default for WeaponType {
    fn default() -> Self {
        WeaponType::None
    }
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Default, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Weapon {
    id: String,
    name: Option<String>,
    model: Option<String>,
    weight: f64,
    value: u64,
    weapon_type: WeaponType,
    health: u64,
    speed: f64,
    reach: f64,
    enchantment_points: u64,
    chop: [u64; 2],
    slash: [u64; 2],
    thrust: [u64; 2],
    #[serde(default, skip_serializing_if = "is_default")]
    traits: LinkedHashSet<WeaponTraits>,
    script: Option<String>,
    icon: Option<String>,
    enchantment: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Weapon> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut weapon: Weapon = Default::default();
        let mut id = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => weapon.name = Some(take_nt_str(data)?.1),
                b"MODL" => weapon.model = Some(take_nt_str(data)?.1),
                b"SCRI" => weapon.script = Some(take_nt_str(data)?.1),
                b"ITEX" => weapon.icon = Some(take_nt_str(data)?.1),
                b"ENAM" => weapon.enchantment = Some(take_nt_str(data)?.1),
                b"WPDT" => {
                    let (data, weight) = le_f32(data)?;
                    let (data, value) = le_i32(data)?;
                    let (data, typ) = le_i16(data)?;
                    let (data, health) = le_i16(data)?;
                    let (data, speed) = le_f32(data)?;
                    let (data, reach) = le_f32(data)?;
                    let (data, enchantment_points) = le_i16(data)?;
                    let (data, (chop_min, chop_max)) = tuple((le_u8, le_u8))(data)?;
                    let (data, (slash_min, slash_max)) = tuple((le_u8, le_u8))(data)?;
                    let (data, (thrust_min, thrust_max)) = tuple((le_u8, le_u8))(data)?;
                    let (_, flags) = le_i32(data)?;

                    weapon.weight = parse_float(weight);
                    weapon.value = value as u64;
                    weapon.weapon_type =
                        FromPrimitive::from_i16(typ).ok_or(RecordError::InvalidWeaponType(typ))?;
                    weapon.health = health as u64;
                    weapon.speed = parse_float(speed);
                    weapon.reach = parse_float(reach);
                    weapon.enchantment_points = enchantment_points as u64;
                    weapon.chop = [chop_min as u64, chop_max as u64];
                    weapon.slash = [slash_min as u64, slash_max as u64];
                    weapon.thrust = [thrust_min as u64, thrust_max as u64];
                    weapon.traits = BitFlags::<WeaponTraits>::from_bits_truncate(flags as u32)
                        .iter()
                        .collect();
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Weapon, id),
            weapon,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Weapon> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.enchantment, "ENAM")?;
        let mut data = vec![];
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        data.extend(&(self.record.weapon_type as i16).to_le_bytes());
        data.extend(&(self.record.health as i16).to_le_bytes());
        data.extend(&(self.record.speed as f32).to_le_bytes());
        data.extend(&(self.record.reach as f32).to_le_bytes());
        data.extend(&(self.record.enchantment_points as i16).to_le_bytes());
        data.extend(self.record.chop.iter().map(|x| *x as u8));
        data.extend(self.record.slash.iter().map(|x| *x as u8));
        data.extend(self.record.thrust.iter().map(|x| *x as u8));
        let mut flags = 0i32;
        for flag in self.record.traits {
            flags |= flag as i32;
        }
        data.extend(&flags.to_le_bytes());
        subrecords.push(Subrecord::new(*b"WPDT", data, false));
        let header = RecordHeader::new(*b"WEAP", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
