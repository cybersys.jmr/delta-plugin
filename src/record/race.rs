/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{parse_attributes, AttributeType, SkillType};
use crate::util::{
    is_default, nt_str, parse_float, push_opt_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(DeltaRecord, Constructor, Default, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AttributeBonus {
    attributes: LinkedHashMap<AttributeType, i32>,
    height: f64,
    weight: f64,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Race {
    name: Option<String>,
    skill_bonuses: LinkedHashMap<SkillType, i64>,
    #[delta]
    male_bonuses: AttributeBonus,
    #[delta]
    female_bonuses: AttributeBonus,
    #[serde(default, skip_serializing_if = "is_default")]
    playable: bool,
    #[serde(default, skip_serializing_if = "is_default")]
    beast_race: bool,
    #[serde(default, skip_serializing_if = "is_default")]
    powers: LinkedHashSet<String>,
    desc: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Race> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut desc = None;
        let mut skill_bonuses = LinkedHashMap::new();
        let mut powers = LinkedHashSet::new();
        let mut race_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            #[nom(Count = "14")]
            skills: Vec<i32>,
            #[nom(Count = "16")]
            attributes: Vec<i32>,
            male_height: f32,
            female_height: f32,
            male_weight: f32,
            female_weight: f32,
            flags: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"RADT" => race_data = Some(Data::parse(data)?.1),
                b"NPCS" => {
                    powers.insert(take_nt_str(data)?.1);
                }
                b"DESC" => desc = Some(take_nt_str(data)?.1),
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let data = race_data.ok_or(RecordError::MissingSubrecord("RADT"))?;
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        for i in 0..7 {
            let (skill, bonus) = (data.skills[2 * i], data.skills[2 * i + 1]);

            if skill != -1 {
                skill_bonuses.insert(skill.try_into()?, bonus as i64);
            }
        }
        let male_bonuses = AttributeBonus::new(
            parse_attributes(data.attributes.clone().into_iter().step_by(2)),
            parse_float(data.male_height),
            parse_float(data.male_weight),
        );
        let female_bonuses = AttributeBonus::new(
            parse_attributes(data.attributes.into_iter().skip(1).step_by(2)),
            parse_float(data.female_height),
            parse_float(data.female_weight),
        );
        let playable = data.flags & 1 != 0;
        let beast_race = data.flags & 2 != 0;

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Race, id),
            Race::new(
                name,
                skill_bonuses,
                male_bonuses,
                female_bonuses,
                playable,
                beast_race,
                powers,
                desc,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Race> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));

        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;

        let mut data = vec![];
        let skill_bonuses_len = self.record.skill_bonuses.len();
        for (skill, bonus) in self.record.skill_bonuses {
            data.extend(&(skill as i32).to_le_bytes());
            data.extend(&(bonus as i32).to_le_bytes());
        }
        // Ensure that exactly seven skill/bonuses are included
        for _ in skill_bonuses_len..7 {
            data.extend(&(-1i32).to_le_bytes());
            data.extend(&(-1i32).to_le_bytes());
        }

        use AttributeType::*;
        for attribute in &[
            Strength,
            Intelligence,
            Willpower,
            Agility,
            Speed,
            Endurance,
            Personality,
            Luck,
        ] {
            data.extend(
                &(*self
                    .record
                    .male_bonuses
                    .attributes
                    .get(attribute)
                    .unwrap_or(&0) as i32)
                    .to_le_bytes(),
            );
            data.extend(
                &(*self
                    .record
                    .female_bonuses
                    .attributes
                    .get(attribute)
                    .unwrap_or(&0) as i32)
                    .to_le_bytes(),
            );
        }
        data.extend(&(self.record.male_bonuses.height as f32).to_le_bytes());
        data.extend(&(self.record.female_bonuses.height as f32).to_le_bytes());
        data.extend(&(self.record.male_bonuses.weight as f32).to_le_bytes());
        data.extend(&(self.record.female_bonuses.weight as f32).to_le_bytes());
        let mut flags = 0;
        if self.record.playable {
            flags |= 1;
        }
        if self.record.beast_race {
            flags |= 2;
        }
        data.extend(&(flags as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"RADT", data, false));

        for power in self.record.powers {
            let mut power_data = nt_str(&power)?;
            power_data.resize(32, 0);
            subrecords.push(Subrecord::new(*b"NPCS", power_data, false));
        }

        push_opt_str_subrecord(&mut subrecords, &self.record.desc, "DESC")?;

        let header = RecordHeader::new(*b"RACE", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
