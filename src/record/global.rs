/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, subrecords_len, take_nt_str, RecordError};
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_f32;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Value {
    Short(i16),
    Long(i32),
    Float(f32),
}

#[derive(Clone, Debug, PartialEq)]
pub enum ValueType {
    Short,
    Long,
    Float,
}

#[derive(OverrideRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Global {
    #[serde(flatten)]
    value: Option<Value>,
}

impl TryFrom<esplugin::Record> for RecordPair<Global> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let mut id = None;
        let mut value_data = None;
        let mut value_type = None;
        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(&data)?.1),
                b"FLTV" => value_data = Some(data),
                b"FNAM" => match data[0] as char {
                    's' => value_type = Some(ValueType::Short),
                    'l' => value_type = Some(ValueType::Long),
                    'f' => value_type = Some(ValueType::Float),
                    x => Err(RecordError::TypeError(format!(
                        "Unexpected global type {}",
                        x
                    )))?,
                },
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let value_type = value_type.ok_or(RecordError::MissingSubrecord("FNAM"))?;
        let value_data = value_data.ok_or(RecordError::MissingSubrecord("FLTV"))?;
        let value = match value_type {
            ValueType::Short => {
                let value = le_f32(value_data)?.1;
                // Interestingly, the way OpenMW/C++ parses these
                // seems to result in large floats (which may be NaN in C++, but aren't here)
                // becoming 0
                if value.is_nan() || value > i16::MAX as f32 {
                    Some(Value::Short(0i16))
                } else {
                    Some(Value::Short(value as i16))
                }
            }
            ValueType::Long => Some(Value::Long(le_f32(value_data)?.1 as i32)),
            ValueType::Float => Some(Value::Float(le_f32(value_data)?.1)),
        };

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Global, id),
            Global { value },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Global> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));
        if let Some(value) = self.record.value {
            match value {
                Value::Short(data) => {
                    subrecords.push(Subrecord::new(*b"FNAM", vec!['s' as u8], false));
                    subrecords.push(Subrecord::new(
                        *b"FLTV",
                        (data as f32).to_le_bytes().to_vec(),
                        false,
                    ));
                }
                Value::Long(data) => {
                    subrecords.push(Subrecord::new(*b"FNAM", vec!['l' as u8], false));
                    subrecords.push(Subrecord::new(
                        *b"FLTV",
                        (data as f32).to_le_bytes().to_vec(),
                        false,
                    ));
                }
                Value::Float(data) => {
                    subrecords.push(Subrecord::new(*b"FNAM", vec!['f' as u8], false));
                    subrecords.push(Subrecord::new(
                        *b"FLTV",
                        (data as f32).to_le_bytes().to_vec(),
                        false,
                    ));
                }
            }
        }

        let header = RecordHeader::new(*b"GLOB", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
