/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate base64_serde;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate num_derive;
#[macro_use]
extern crate serde_with;
#[macro_use]
extern crate delta_plugin_derive;
#[macro_use]
extern crate log;
#[macro_use]
extern crate nom_derive;
#[macro_use]
extern crate enum_dispatch;
#[macro_use]
extern crate enumflags2;

#[macro_use]
mod delta;
#[macro_use]
mod types;
#[macro_use]
mod util;

mod ai;
mod filter;
mod float;
mod patch;
mod plugin;
mod serialize;
mod version;

mod record;

use crate::patch::{apply, diff};
use crate::plugin::Plugin;
use crate::record::{MetaRecord, RecordType};
use crate::serialize::{apply_delta, create_delta, create_merged, load_plugin};
use anyhow::{Context, Result};
use clap::{arg, command, value_parser, Arg, ArgAction, Command};
use esplugin::GameId;
use fancy_regex::Regex;
use openmw_cfg::{find_file_or_archive, get_config, read_file, VFSFile};
use std::borrow::Cow;
use std::collections::HashSet;
use std::convert::TryInto;
use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::str::FromStr;

fn write_plugin(plugin: Plugin) -> Result<()> {
    let filename = plugin.file.clone();
    let new_plugin: esplugin::Plugin = plugin.try_into().context(format!(
        "Failed to convert plugin {} to esm",
        filename.display()
    ))?;
    println!("Writing to file {}...", filename.display());
    new_plugin.write_file()?;
    Ok(())
}

fn convert(filename: &Path, inline: bool, compare: bool) -> Result<()> {
    let ext = "yaml";
    match filename.extension().map(|x| x.to_str()) {
        Some(Some("yaml")) => {
            let mut delta_plugin: Plugin = load_plugin(filename)?;
            delta_plugin.file = filename.with_extension("omwaddon");
            trace!("Converting...");
            let masters = serialize::load_masters(&delta_plugin).context(format!(
                "Failed to load masters of plugin {}",
                filename.display()
            ))?;
            let plugin = apply_delta(delta_plugin, &masters.iter().collect::<Vec<_>>());
            write_plugin(plugin)?;
        }
        Some(_) | None => {
            let plugin = load_plugin(filename)?;
            let out_dir = plugin.file.with_extension("d");

            let mut plugin = if compare {
                let masters = serialize::load_masters(&plugin).context(format!(
                    "Failed to load masters of plugin {}",
                    filename.display()
                ))?;
                let delta_plugin =
                    create_delta(Cow::Owned(plugin), &masters.iter().collect::<Vec<_>>());

                delta_plugin
            } else {
                plugin
            };

            let mut has_script = false;
            if !inline {
                // Check plugin for scripts and write them to a separate file
                for record in &mut plugin.records {
                    if let (id, MetaRecord::Record(RecordType::Script(ref mut script))) = record {
                        has_script = true;
                        script.create_script_file(id, &out_dir).context(format!(
                            "Failed to create script file for script \"{}\"",
                            id
                        ))?;
                    }
                }
            }
            let out_str = serde_yaml::to_string(&plugin).context(format!(
                "Failed to serialize plugin {} as yaml",
                filename.display()
            ))?;

            // Emit includes by replacing the quotes surrounding them.
            let re = Regex::new("\"!(?P<y>\\w*)\\s*\\\\\"(?P<x>[^\"]*)\\\\\"\"").unwrap();
            let out_str = re.replace_all(&out_str, "!$y \"$x\"").into_owned();

            if has_script {
                let file_name = Path::new(plugin.file.file_name().unwrap()).with_extension(ext);

                println!(
                    "Writing result to {}...",
                    out_dir.join(&file_name).display()
                );
                fs::write(out_dir.join(&file_name), out_str)
                    .context(format!("Failed to write file {}", &file_name.display()))?;
            } else {
                let path = plugin.file.with_extension(ext);
                println!("Writing result to {}...", path.display());
                fs::write(&path, out_str)
                    .context(format!("Failed to write file {}", &path.display()))?;
            }
        }
    }
    Ok(())
}

fn get_deps(path: &Path) -> Result<Vec<String>> {
    // Assert file exists and is readable
    fs::metadata(path).context(format!("When reading file \"{}\"", path.display()))?;

    for id in &[
        GameId::Morrowind,
        GameId::Oblivion,
        GameId::Skyrim,
        GameId::Fallout3,
        GameId::FalloutNV,
        GameId::Fallout4,
        GameId::SkyrimSE,
    ] {
        let mut plugin = esplugin::Plugin::new(*id, path);
        if plugin.parse_file(esplugin::ParseMode::HeaderOnly).is_ok() {
            // If the plugin is well-formed, masters should work. It won't fail even if the wrong
            // gameid is being used, since they all use the MAST subrecord, and that's all that's
            // being parsed
            let list = plugin.masters().map_err(|x| {
                anyhow!("Error reading masters for plugin {}: {}", path.display(), x)
            })?;
            return Ok(list);
        }
    }
    Err(anyhow!(
        "Unable to parse dependencies for file {}",
        path.display()
    ))
}

fn cli() -> Command {
    command!()
        .arg(Arg::new("verbose").short('v').long("verbose").action(ArgAction::Count).help("Sets the level of verbosity,\
            which is higher the more times this argument is repeated").required(false))
        .arg(Arg::new("quiet").short('q').long("quiet").action(ArgAction::SetTrue).help("Run in quiet mode").required(false))
        .arg(Arg::new("openmw-cfg").short('c').value_name("FILE").value_parser(value_parser!(PathBuf)).required(false))
        .subcommand(Command::new("convert")
            .about("Converts files from esp to yaml-encoded Delta and vice versa")
            .arg(arg!([PLUGINS] ... "Files to be converted").value_parser(value_parser!(PathBuf)))
            .arg(arg!(-i --inline "Produces a single file as output, \
            inlining information such as scripts which would normally be written to separate files."))
            .arg(arg!(-c --compare "Don't compare recoords with their masters \
                and instead produce a 1-to-1 representation of the original.
                Only applies when converting esm to yaml")))
        .subcommand(Command::new("merge")
            .about("Creates a merged plugin for the current plugin configuration as defined in openmw.cfg")
            .arg(arg!([out_file] "location of the resulting merged plugin. Default is ./merged.omwaddon")
                .value_parser(value_parser!(PathBuf)))
            .arg(arg!(--"debug-incremental" "Builds in incremental mode, dumping intermediate representations \
            to ~/.cache/delta_plugin").required(false))
            .arg(Arg::new("ignore").long("ignore")
                .help("Plugin name to ignore when loading plugins.").action(ArgAction::Append))
            .arg(arg!(--"skip-cells" "Ignore Cell records when creating the merged plugin. Merging cells should \
                be stable, but it is a time and memory-intensive process, so if you have issues with running out of memory \
                you may want to omit cells from the merged plugin.").required(false)))
        .subcommand(Command::new("diff")
            .about("Diffs two plugin files and creates a unified text diff representing the difference between the two files")
            .arg(arg!(<original> "location of the plugin to base the diff off of").value_parser(value_parser!(PathBuf)))
            .arg(arg!(<new> "location of the new version of the plugin").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("apply")
            .about("Applies a patch. The source and destination file should be relative to the current directory.")
            .arg(arg!(<patch> "patch file to apply").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("deps")
            .about("Produces a newline separated list of plugin dependencies. Unlike other commands, this supports all esp formats.")
            .arg(arg!(<plugin> "Plugin to parse. Should be a binary esp/esm/esl of any type.").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("size")
            .about("Prints the number of records in the given plugin. This supports all esp formats.")
            .arg(arg!(<plugin> "Plugin to parse. Should be a binary esp/esm/esl of any type.").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("filter")
            .about("Filters and dumps data from the load order. \
                Either --all or --input must be provided. \
                Is followed by one or more match statement indicating records to include.")
            .arg(Arg::new("all").short('a').long("all").action(ArgAction::SetTrue)
                .help("Read from all plugins in openmw.cfg").conflicts_with("input"))
            .arg(Arg::new("desc").long("desc")
                .help("Description to include in the output plugin header"))
            .arg(Arg::new("author").long("author")
                .help("Author to include in the output plugin header"))
            .arg(Arg::new("ignore").long("ignore")
                .help("Plugin name to ignore when loading plugins.").action(ArgAction::Append))
            .arg(Arg::new("input").short('i').long("input").value_parser(value_parser!(PathBuf)).action(ArgAction::Append)
                .required_unless_present("all")
                .help("Input plugin to filter. Can be specified multiple times, to read from the given plugins in order"))
            .arg(Arg::new("output").short('o').long("output").value_parser(value_parser!(PathBuf)).help("Output plugin for the filter data. \
                 If omitted, writes to output.omwaddon"))
            // Note: the arguments are manually parsed to allow multiple match subcommands to be used
            // simultaneously.
            .subcommand(Command::new("match")
                .arg(Arg::new("type").value_parser(crate::record::RecordTypeName::from_str).help("Record type to match."))
                .arg(Arg::new("id").long("id").value_parser(value_parser!(Regex))
                    .help("Only includes records with an id matching the provided regex"))
                .arg(Arg::new("cellref-object-id").long("cellref-object-id").value_parser(value_parser!(Regex))
                    .help("Only includes cell references with an object_id matching the provided regex"))
                .arg(arg!(--delete "Turns matched records into deleted records").required(false))
                .arg(Arg::new("modify").long("modify").help("Modifies records matching the given field and value regex. \
                        Note that replacements can capture strings in the regex as described here: \
                        https://docs.rs/regex/latest/regex/struct.Regex.html#replacement-string-syntax")
                    .value_names(["field", "regex", "substitution"]).required(false).num_args(3)))
            )
        .subcommand(Command::new("query")
            .about("Filters and dumps data from the load order. \
                Either --all or --input must be provided. \
                Is followed by one or more match statement indicating records to include. ")
            .arg(Arg::new("ids").long("ids").action(ArgAction::SetTrue)
                .help("Only output the ids of matching records"))
            .arg(Arg::new("count").long("count").action(ArgAction::SetTrue)
                .help("Only output the count of matching records").conflicts_with("ids"))
            .arg(Arg::new("all").short('a').long("all").action(ArgAction::SetTrue)
                .help("Read from all plugins in openmw.cfg").conflicts_with("input"))
            .arg(Arg::new("input").short('i').long("input").value_parser(value_parser!(PathBuf)).action(ArgAction::Append)
                .required_unless_present("all")
                .help("Input plugin to filter. Can be specified multiple times, to read from the given plugins in order"))
            .arg(Arg::new("ignore").long("ignore")
                .help("Plugin name to ignore when loading plugins.").action(ArgAction::Append))
            .subcommand(Command::new("match")
                .arg(Arg::new("type").value_parser(crate::record::RecordTypeName::from_str).help("Record type to match."))
                .arg(Arg::new("id").long("id").value_parser(value_parser!(Regex))
                    .help("Only includes records with an id matching the provided regex"))
                .arg(Arg::new("cellref-object-id").long("cellref-object-id").value_parser(value_parser!(Regex))
                    .help("Only includes cell references with an object_id matching the provided regex"))))
        .subcommand(Command::new("vfs-find").about("Prints the absolute path of a given file within the OpenMW VFS. \
                If the file is inside a BSA archive, prints the path to the archive.")
            .arg(Arg::new("file").help("File to search for")))
        .subcommand(Command::new("vfs-extract").about("Copies the data from the given file in the VFS to the output path")
            .arg(Arg::new("path").help("File to search for"))
            .arg(Arg::new("output").help("Path of the file to write"))
            )
}

#[test]
fn verify_cli() {
    cli().debug_assert();
}

fn set_verbosity(matches: &clap::ArgMatches) -> u8 {
    let verbosity = if matches.get_flag("quiet") {
        0
    } else {
        1 + matches.get_count("verbose")
    };
    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity as usize)
        .init()
        .unwrap();
    verbosity
}

fn set_openmw_cfg(matches: &clap::ArgMatches) {
    if let Some(path) = matches.get_one::<PathBuf>("openmw-cfg") {
        env::set_var("OPENMW_CONFIG", path);
    }
}

fn cli_filter(args: Vec<String>) -> Result<()> {
    // Pre-process arguments, then call get_matches_from multiple times on each filter
    // subcommand

    let mut split = args.split(|elem| elem == "match");
    let base_args: &[String] = split.next().unwrap();
    trace!("Base args: {:?}", &base_args);
    let mut subcommand_invocations: Vec<Vec<String>> = vec![];
    for group in split {
        subcommand_invocations.push(group.to_vec());
    }

    let matches = cli().get_matches_from(base_args);
    set_verbosity(&matches);
    set_openmw_cfg(&matches);

    let filter_input = if let Some(matches) = matches
        .subcommand_matches("filter")
        .or(matches.subcommand_matches("query"))
    {
        if matches.get_flag("all") {
            crate::filter::FilterInput::All
        } else {
            crate::filter::FilterInput::Plugins(
                matches
                    .get_many::<PathBuf>("input")
                    .unwrap()
                    .cloned()
                    .collect(),
            )
        }
    } else {
        unreachable!()
    };

    let (is_filter, output) = if let Some(matches) = matches.subcommand_matches("filter") {
        (
            true,
            Some(
                matches
                    .get_one::<PathBuf>("output")
                    .map(|x| x.as_path())
                    .unwrap_or(Path::new("output.omwaddon")),
            ),
        )
    } else {
        (false, None)
    };

    let string = "match".to_string();
    let mut filters = vec![];
    for group in subcommand_invocations {
        let mut args: Vec<&String> = base_args.iter().collect();
        // We split on the match, which removed it. Re-add here.
        args.push(&string);
        trace!("Args: {:?}", &args);
        let matches = cli().get_matches_from(args.into_iter().chain(group.iter()));
        if let Some(matches) = matches
            .subcommand_matches("filter")
            .or(matches.subcommand_matches("query"))
        {
            if let Some(matches) = matches.subcommand_matches("match") {
                // Parse modify, list of two results, into a field name and regex tuple.
                let (modify, delete) = if is_filter {
                    let modify: Option<Vec<String>> =
                        matches.get_many("modify").map(|x| x.cloned().collect());
                    let modify: Result<Option<(String, Regex, String)>> = modify
                        .map(|modify| {
                            Ok(Some((
                                modify[0].clone(),
                                Regex::new(&modify[1])?,
                                modify[2].clone(),
                            )))
                        })
                        .unwrap_or(Ok(None));
                    (modify?, matches.get_flag("delete"))
                } else {
                    (None, false)
                };

                filters.push(crate::filter::Filter {
                    record_type: *matches
                        .get_one::<crate::record::RecordTypeName>("type")
                        .unwrap(),
                    id: matches.get_one::<Regex>("id").cloned(),
                    cellref_object_id: matches.get_one::<Regex>("cellref-object-id").cloned(),
                    delete,
                    modify,
                });
            }
        }
    }
    let mut ignored_plugins: HashSet<&str> =
        if let Some(ignored) = matches.get_many::<&str>("ignore") {
            ignored.copied().collect()
        } else {
            HashSet::new()
        };
    if matches.subcommand_matches("filter").is_some() {
        let output_filename = output
            .unwrap()
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .to_lowercase();
        ignored_plugins.insert(output_filename.as_str());
        let plugin = crate::filter::filter_plugin(
            filter_input,
            output.unwrap(),
            filters,
            matches.get_one::<String>("author").cloned(),
            matches.get_one::<String>("desc").cloned(),
            ignored_plugins,
        )?;
        write_plugin(plugin)?;
    } else if let Some(subcommand_matches) = matches.subcommand_matches("query") {
        let ordered_plugins =
            crate::filter::get_ordered_plugins_from_input(filter_input, ignored_plugins)?;
        let records = crate::filter::filter_records(ordered_plugins, filters)?;
        // FIXME: Not displaying anything!
        if subcommand_matches.get_flag("count") {
            println!("{}", records.len());
        } else if subcommand_matches.get_flag("ids") {
            for (id, _) in records {
                // If id is a typed string, ignore the type
                if let crate::record::RecordId::String(_, id) = id {
                    println!("{id}");
                } else {
                    println!("{id}");
                }
            }
        } else {
            println!(
                "{}",
                serde_yaml::to_string(&records)
                    .context("Converting output records to yaml")?
                    .strip_prefix("---\n")
                    .unwrap()
            );
        }
    }
    Ok(())
}

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    // The first positional argument should be the subcommand.
    // If it's filter, use the special filter parser
    trace!("All Args: {:?}", &args);
    if args.iter().any(|x| *x == "--help") {
        cli().get_matches_from(&args);
    }
    let mut iter = args[1..].iter();
    while let Some(elem) = iter.next() {
        if !elem.starts_with('-') && elem != "filter" && elem != "query" {
            break;
        }
        if elem == "--openmw-cfg" || elem == "-c" {
            // Skip argument
            iter.next();
        }
        if elem == "filter" || elem == "query" {
            return cli_filter(args);
        }
    }
    let matches = cli().get_matches_from(args);
    // TODO: Additional command line options, such as specifying the location of the cache directory,
    // specifying explicit paths of the plugins to be merged and overriding the encoding to be used
    // when parsing plugin files.

    let verbosity = set_verbosity(&matches);
    set_openmw_cfg(&matches);

    if let Some(matches) = matches.subcommand_matches("convert") {
        if let Some(plugins) = matches.get_many::<PathBuf>("PLUGINS") {
            for filename in plugins {
                convert(
                    filename,
                    matches.get_flag("inline"),
                    matches.get_flag("compare"),
                )?;
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("diff") {
        let original = matches
            .get_one::<PathBuf>("original")
            .ok_or(anyhow!("Missing argument <original>"))?;
        let new = matches
            .get_one::<PathBuf>("new")
            .ok_or(anyhow!("Missing argument <new>"))?;

        let result = diff(original, new)?;
        if !result.is_empty() {
            println!("{}", result);
        }
    } else if let Some(matches) = matches.subcommand_matches("apply") {
        apply(
            matches
                .get_one::<PathBuf>("patch")
                .ok_or(anyhow!("Missing argument <patch>"))?,
        )?;
    } else if let Some(matches) = matches.subcommand_matches("merge") {
        let filename: PathBuf = matches
            .get_one::<PathBuf>("out_file")
            .unwrap_or(
                &std::env::current_dir()
                    .context("Current directory could not be read.")?
                    .join("merged.omwaddon"),
            )
            .to_path_buf();
        let mut ignored_plugins: HashSet<&str> =
            if let Some(ignored) = matches.get_many::<String>("ignore") {
                ignored.map(|x| x.as_str()).collect()
            } else {
                HashSet::new()
            };

        let output_filename = filename
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .to_lowercase();
        ignored_plugins.insert(output_filename.as_str());
        let merged = create_merged(
            &filename,
            matches.get_flag("debug-incremental"),
            matches.get_flag("skip-cells"),
            verbosity > 1,
            ignored_plugins,
        )
        .context(format!(
            "Failed to create merged plugin {}",
            filename.display()
        ))?;
        println!("Writing merged plugin to {}", filename.display());
        let bin: esplugin::Plugin = merged.try_into().context(format!(
            "Failed to serialize plugin {} as esm",
            filename.display()
        ))?;
        bin.write_file()
            .context(format!("Failed to write file {}", filename.display()))?;
        println!("Done!");
    } else if let Some(matches) = matches.subcommand_matches("deps") {
        let path = matches.get_one::<PathBuf>("plugin").unwrap();
        for dep in get_deps(path)? {
            println!("{}", dep);
        }
    } else if let Some(matches) = matches.subcommand_matches("size") {
        let filename = matches.get_one::<PathBuf>("plugin").unwrap();
        for id in &[
            GameId::Morrowind,
            GameId::Oblivion,
            GameId::Skyrim,
            GameId::Fallout3,
            GameId::FalloutNV,
            GameId::Fallout4,
            GameId::SkyrimSE,
        ] {
            let mut plugin = esplugin::Plugin::new(*id, Path::new(&filename));
            if plugin.parse_file(esplugin::ParseMode::HeaderOnly).is_ok() {
                if let Some(records) = plugin.record_and_group_count() {
                    println!("{}", records);
                    break;
                }
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("vfs-find") {
        let ini = get_config()?;
        let path = match find_file_or_archive(&ini, matches.get_one::<String>("file").unwrap())? {
            VFSFile::File(path) => path,
            VFSFile::ArchivedFile(path) => path,
        };
        println!("{}", path.display());
    } else if let Some(matches) = matches.subcommand_matches("vfs-extract") {
        let ini = get_config()?;
        let data = read_file(&ini, matches.get_one::<String>("path").unwrap())?;
        let path = matches.get_one::<String>("output").unwrap();
        std::fs::write(path, data)?;
    }

    Ok(())
}
